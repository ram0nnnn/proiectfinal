FROM alpine:latest

RUN apk update && apk add python3-dev
RUN mkdir -p /var/logs/wantsome/
RUN touch /var/logs/wantsome/HTTPServer.log
RUN touch /var/logs/wantsome/HTTPWorkers.log
WORKDIR /home

COPY webserver.py /home/webserver.py

RUN chmod 744 /home/webserver.py

ENTRYPOINT ["python3","./webserver.py"]

CMD ["start","--host","0.0.0.0","--port","8080","--workers","2"]

