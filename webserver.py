#!/usr/bin/env python3
"""Simple HTTP Echo Server."""
import math
import argparse
import errno
import time
import queue
import logging
import os
import select
import socket
import threading


LOGS_PATH = '/var/logs/wantsome'


def _get_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    
    # create file handler which logs even debug messages
    file_handler = logging.FileHandler('%s/%s.log' % (LOGS_PATH, name))
    file_handler.setLevel(logging.DEBUG)
    
    # create console handler with a higher log level
    steam_handler = logging.StreamHandler()
    steam_handler.setLevel(logging.ERROR)
    
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(threadName)s '
                                  '- %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    steam_handler.setFormatter(formatter)
    
    # add the handlers to the logger
    logger.addHandler(file_handler)
    logger.addHandler(steam_handler)

    return logger


class _Worker(object):

    """Abstract base class for simple workers."""

    def __init__(self, tasks_queue, stop_event, delay=0.1):
        super(_Worker, self).__init__()
        self._queue = tasks_queue
        self._stop = stop_event
        self._delay = delay
        
        self._name = self.__class__.__name__
        self._logger = _get_logger(self._name)

        self.run()

    def _get_task(self):
        """Retrieves a task from the queue."""
        while not self._stop.is_set():
            try:
                task = self._queue.get(block=False)
                if task:
                    return task
            except queue.Empty:
                time.sleep(self._delay)

    def _task_done(self, task, result):
        """What to execute after successfully finished processing a task."""
        pass

    def _task_fail(self, task, exc):
        """What to do when the program fails processing a task."""
        self._logger.error("The %r task failed.", task)
        self._logger.error(str(exc))

    def _process(self, task):
        """Override this with your custom worker logic."""
        pass

    def run(self):
        """Worker able to retrieve and process tasks."""
        self._logger.debug("The worker is starting...")
        while not self._stop.is_set():
            task = self._get_task()
            try:
                result = self._process(task)
            except Exception as exc:
                self._task_fail(task, exc)
            else:
                self._task_done(task, result)


class _Daemon(object):

    """Abstract base class for simple daemons."""

    def __init__(self, delay=1, workers=7):
        """Setup a new instance."""
        self._delay = delay
        self._workers_count = workers
        self._workers = list()
        self._manager = None
        self._queue = queue.Queue()
        self._stop = threading.Event()

        self._name = self.__class__.__name__
        self._logger = _get_logger(self._name)

    def _start_worker(self):
        """Creates a new worker."""
        pass

    def _task_generator(self):
        """Override this with your custom task generator."""
        pass

    def manage_workers(self):
        """Maintain a desired number of workers up."""
        while not self._stop.is_set():
            for worker in self._workers[:]:
                if not worker.is_alive():
                    self._workers.remove(worker)

            if len(self._workers) == self._workers_count:
                time.sleep(self._delay)
                continue

            worker = self._start_worker()
            self._workers.append(worker)

    def _interrupted(self):
        """Mark the processing as stopped."""
        self._stop.set()

    def _put_task(self, task):
        """Adds a task to the queue."""
        self._queue.put(task)

    def _prologue(self):
        """Start a parallel supervisor."""
        self._manager = threading.Thread(target=self.manage_workers)
        self._manager.start()

    def _epilogue(self):
        """Wait for that supervisor and its workers."""
        self._logger.info("The server is shuting down...")
        if self._manager:
            self._logger.debug("Waiting for the management thread...")
            self._manager.join()

        self._logger.debug("Waiting for workers...")
        for worker in self._workers:
            if worker.is_alive():
                worker.join()

    def start(self):
        """Starts a series of workers and processes incoming tasks."""
        self._prologue()
        while not self._stop.is_set():
            try:
                for task in self._task_generator():
                    self._put_task(task)
            except KeyboardInterrupt:
                self._interrupted()
                break
        self._epilogue()


class HTTPWorker(_Worker):

    """Simple HTTP echo worker."""

    def _get_response(self, content, response_code="200 OK"):
        headers = {
            "Date": time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()),
            "Server": self._name,
            "Connection": "close",
            "X-Laborator": "Wantsome - HTTP",
            "Content-Length": len(content),
            "Content-Type": "text/plain",
        }
        output = ["HTTP/1.1 {response}".format(response=response_code)]
        for key, value in headers.items():
            output.append("{key}: {value}".format(key=key.title(),
                                                  value=value))
        output.append("\n")
        output.append(content)
        return "\n".join(output)

    def _task_done(self, task, result):
        """Send the result to the client."""
        response = self._get_response(result, "200 OK")
        task.sendall(response.encode())
        task.close()

    def _task_fail(self, task, exc):
        """Send an error to the client"""
        if not task:
            return
        task.close()

    def _process(self, task):
        """Receive the information from the client."""
        request = []
        while True:
            chunk = task.recv(1024)
            request.append(chunk.decode())
            if len(chunk) < 1024:
                break

        return "".join(request)


class HTTPServer(_Daemon):

    """Simple HTTP Server."""

    def __init__(self, host, port, backlog=7, delay=1, workers=7):
        super(HTTPServer, self).__init__(delay=delay,workers=workers)
        self._port = port
        self._host = host
        self._backlog = backlog
        self._socket = None

    def _start_worker(self):
        """Start a new HTTP Worker."""
        self._logger.info("Starting a new worker...")
        worker = threading.Thread(target=HTTPWorker,
                                  args=(self._queue, self._stop))
        worker.setDaemon(True)
        worker.start()
        return worker

    def _task_generator(self):
        """Listen for new connection and pass them to the workers."""
        read_list, write_list, error_list = [self._socket], [], []
        while not self._stop.is_set():
            readables, _, _ = select.select(read_list, write_list, error_list)
            if self._socket not in readables:
                continue
            try:
                connection, _ = self._socket.accept()
            except IOError as exc:
                code, _ = exc.args
                if code == errno.EINTR:
                    continue
            yield connection

    def _prologue(self):
        """Setup the socket."""
        self._logger.info("The HTTP server is starting...")
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.setblocking(0)

        try:
            self._socket.bind((self._host, self._port))
            self._socket.listen(self._backlog)
        except OSError as exc:
            self._logger.error("Failed to start server: %s", exc)
            self._stop.set()
            return

        self._logger.info("The server is listening on %s:%s",
                          self._host, self._port)
        super(HTTPServer, self)._prologue()


def main():
    """The entrypoint of the current script."""
    global LOGS_PATH

    base_parser = argparse.ArgumentParser()
    commands = base_parser.add_subparsers(
            title="[commands]", dest="command")

    parser = commands.add_parser(
        "start",
        help="Start the echo server.")

    host = os.environ.get('HOST', 'xxx.xxx.xxx.xxx')
    parser.add_argument(
        "--host", type=str, default=host,
        help="The IP address or the host name of the server. "
             "Default: %s" % host
    )

    port = int(os.environ.get('PORT', '1'))
    parser.add_argument(
        "--port", type=int, default=port,
        help="The port that should be used by the current web service. "
             "Default: %s" % port
    )

    workers = int(os.environ.get('WORKERS', '4096'))
    parser.add_argument(
        "--workers", type=int, default=workers,
        help="The number of thread workers used in order to serve "
             "clients. Default: %s" % workers
    )

    logs_path = os.environ.get('LOGS_PATH', LOGS_PATH)
    parser.add_argument(
        "--logs-path", type=str, default=logs_path,
        help="Default: %s" % logs_path
    )

    arguments = base_parser.parse_args()
    if 'command' not in arguments:
        base_parser.print_help()
        return

    if arguments.command == 'start':
        LOGS_PATH = arguments.logs_path
        logging.basicConfig(level=logging.DEBUG)
        server = HTTPServer(host=arguments.host,
                            port=arguments.port,
                            workers=arguments.workers)
        server.start()


if __name__ == "__main__":
    main()
